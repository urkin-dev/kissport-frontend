import React from 'react'
import 'reflect-metadata'
import ReactDOM from 'react-dom'
import { history } from '@lib/utils/'
import './styles/styles.css'
import App from './App'
import { CustomRouter } from './CustomRouter'

ReactDOM.render(
	<React.StrictMode>
		<CustomRouter history={history}>
			<App />
		</CustomRouter>
	</React.StrictMode>,
	document.getElementById('root')
)
