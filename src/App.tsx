import { AuthPage, useAuthStore } from '@feature/auth'
import { ErrorScreen, MainScreen } from '@feature/common'
import { ProductListScreen, ProductScreen } from '@feature/product'
import { CartScreen } from '@feature/user'
import { RoutesEnum } from '@lib/routes'
import { useEffect } from 'react'
import { Route, Routes } from 'react-router-dom'

function App() {
	const authStore = useAuthStore()

	useEffect(() => {
		authStore.restoreSession()
	}, [])

	return (
		<>
			<Routes>
				<Route path={RoutesEnum.Auth} element={<AuthPage />} />
				<Route path={RoutesEnum.Home} element={<MainScreen />} />
				<Route path={RoutesEnum.ProductsList} element={<ProductListScreen />} />
				<Route path={RoutesEnum.CartScreen} element={<CartScreen />} />
				<Route path={RoutesEnum.ProductScreen + '/:id'} element={<ProductScreen />} />
				<Route path={RoutesEnum.Error} element={<ErrorScreen />} />
			</Routes>
		</>
	)
}

export default App
