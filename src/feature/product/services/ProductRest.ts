import { BaseRest, IPagination } from '@lib/services'
import { singleton } from 'tsyringe'
import { IProduct } from '../models/Product'

@singleton()
export class ProductRest extends BaseRest {
	async getAllProducts() {
		try {
			const response = await this.http.get<IPagination<IProduct>>('/products/', { withCredentials: true })

			return response
		} catch (err) {
			throw err
		}
	}

	async getProduct(id: number) {
		try {
			const response = await this.http.get<IProduct>(`/products/${id}`, { withCredentials: true })

			return response
		} catch (err) {
			throw err
		}
	}
}
