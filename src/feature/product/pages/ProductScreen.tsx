import { useProductStore } from '../store/ProductStore'
import { observer } from 'mobx-react-lite'
import { ScreenContainer } from '@lib/components'
import { Alert, COLORS, FONT, Typography } from '@kissport/ui'
import styled from 'styled-components'
import { Price } from '@lib/utils'
import { Button } from 'antd'
import { useParams } from 'react-router-dom'
import { useEffect } from 'react'
import { useUserStore } from '@feature/user'
import { useAlert } from '@lib/hooks'

export const ProductScreen = observer(() => {
	const productStore = useProductStore()
	const userStore = useUserStore()
	const params = useParams()
	const alert = useAlert()

	useEffect(() => {
		productStore.getProduct(Number(params.id))
	}, [params])

	if (!productStore.selected) {
		return null
	}

	const addToCart = () => {
		if (productStore.selected) {
			alert.showAlert(`${productStore.selected.productName} добавлен в корзину`)
			userStore.addProduct(productStore.selected)
		}
	}

	return (
		<ScreenContainer>
			<Content>
				<Typography tag="p" type="small" color={COLORS.Black}>
					{productStore.selected.category}
				</Typography>
				<Title tag="h1" type="h1" color={COLORS.GreenDarken}>
					{productStore.selected.productName}
				</Title>
				<Typography tag="p" type="medium" color={COLORS.GreenBase}>
					{productStore.selected.description}
				</Typography>
				<StyledPrice tag="p" type="h3" weight="bold" color={COLORS.GreenLighten}>
					{Price.formatToRubles(productStore.selected.cost)}
				</StyledPrice>
				<CustomButton htmlType="submit" type="primary" onClick={addToCart}>
					Добавить в корзину
				</CustomButton>
			</Content>
			{alert.visible && <Alert message={alert.alertMessage} isCritical={alert.errors?.code === 500} />}
		</ScreenContainer>
	)
})

const Content = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	padding-left: 150px;
`

const Title = styled(Typography)`
	margin-top: 10px;
	margin-bottom: 10px;
`

const StyledPrice = styled(Typography)`
	margin-top: 20px;
`

const CustomButton = styled(Button)`
	margin-top: 30px;
	background-color: ${COLORS.GreenBase};
	color: ${COLORS.White};
	width: 40%;
	height: 50px;
	border: none;
	border-radius: 7px;
	font-size: ${FONT.large}px;
	transition: background-color 0.5s, opacity 0.3s;
	cursor: pointer;

	&:hover {
		opacity: 0.7;
	}
`
