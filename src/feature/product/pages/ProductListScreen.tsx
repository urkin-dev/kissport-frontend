import { useProductStore } from '../store/ProductStore'
import { useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import { ScreenContainer } from '@lib/components'
import { Alert, COLORS, Typography } from '@kissport/ui'
import styled from 'styled-components'
import { ProductCard } from '../components/ProductCard'
import { IProduct } from '../models/Product'
import { RoutesEnum } from '@lib/routes'
import { history } from '@lib/utils'
import { useUserStore } from '@feature/user'
import { useAlert } from '@lib/hooks'

export const ProductListScreen = observer(() => {
	const productStore = useProductStore()
	const userStore = useUserStore()
	const alert = useAlert()

	useEffect(() => {
		productStore.getAllProducts()
	}, [])

	const goToProductScreen = (product: IProduct) => {
		productStore.setSelected(product)
		history.push(RoutesEnum.ProductScreen + `/${product.id}`)
	}

	const addToCart = (product: IProduct) => {
		alert.showAlert(`${product.productName} добавлен в корзину`)
		userStore.addProduct(product)
	}

	return (
		<ScreenContainer>
			<Content>
				<Typography tag="h3" type="h3" color={COLORS.GreenDarken}>
					Каталог
				</Typography>
				<ProductContainer>
					{productStore.products &&
						productStore.products.map((product, index) => (
							<ProductCard
								key={product.productName + index}
								data={product}
								onClick={goToProductScreen}
								onAddProduct={addToCart}
							/>
						))}
				</ProductContainer>
			</Content>
			{alert.visible && <Alert message={alert.alertMessage} isCritical={alert.errors?.code === 500} />}
		</ScreenContainer>
	)
})

const Content = styled.div`
	padding-left: 150px;
`

const ProductContainer = styled.div`
	display: grid;
	grid-template-columns: repeat(4, 1fr);
`
