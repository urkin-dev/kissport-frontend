import { COLORS, FONT, Typography } from '@kissport/ui'
import { Price } from '@lib/utils'
import { Button } from 'antd'
import styled from 'styled-components'
import { IProduct } from '../models/Product'

interface IProps {
	data: IProduct
	onClick: (product: IProduct) => void
	onAddProduct: (product: IProduct) => void
}

export const ProductCard = ({ data, onClick, onAddProduct }: IProps) => {
	const onCardClick = () => onClick(data)

	const onAdd = (e: React.MouseEvent<HTMLDivElement>) => {
		onAddProduct(data)
		e.stopPropagation()
	}

	return (
		<Container onClick={onCardClick}>
			<Category tag="p" type="tiny" color={COLORS.GreenDarken}>
				{data.category}
			</Category>
			<CenteredText tag="p" type="large" weight="bold">
				{data.productName}
			</CenteredText>
			<Cost tag="p" type="medium">
				{Price.formatToRubles(data.cost)}
			</Cost>
			{data.description && (
				<Description tag="p" type="medium" maxLength={112}>
					{data.description}
				</Description>
			)}
			<CustomButton htmlType="button" type="primary" onClick={onAdd}>
				Добавить в корзину
			</CustomButton>
		</Container>
	)
}

const Category = styled(Typography)`
	margin-bottom: 10px;
`

const CenteredText = styled(Typography)`
	text-align: center;
	color: ${COLORS.GreenDarken};
	transition: 0.5s;
`

const Cost = styled(CenteredText)`
	margin-top: 8px;
	color: ${COLORS.GreenBase};
`

const Description = styled(Typography)`
	margin-top: 8px;
	word-wrap: break-word;
	color: ${COLORS.Black};
	transition: 0.5s;
	margin-bottom: 20px;
`

// TODO: Button UI component
const CustomButton = styled(Button)`
	margin-top: auto;
	background-color: ${COLORS.GreenBase};
	color: ${COLORS.White};
	width: 100%;
	height: 50px;
	border: none;
	border-radius: 7px;
	font-size: ${FONT.large}px;
	transition: background-color 0.5s, opacity 0.3s;
	cursor: pointer;

	&:hover {
		opacity: 0.7;
	}
`

const Container = styled.div`
	display: flex;
	flex-direction: column;
	margin-top: 15px;
	border: 1px solid ${COLORS.GreenBase};
	padding: 15px;
	width: 300px;
	cursor: pointer;
	transition: 0.5s;

	&:hover {
		background-color: ${COLORS.GreenBase};

		& > ${CenteredText} {
			color: ${COLORS.White};
		}

		& > ${Cost}, & > ${Description} {
			color: ${COLORS.GrayBase};
		}

		& > ${CustomButton} {
			background-color: ${COLORS.GreenLighten};
		}
	}
`
