import { makeAutoObservable } from 'mobx'
import { container, singleton } from 'tsyringe'
import { IProduct } from '../models/Product'
import { ProductRest } from '../services/ProductRest'

@singleton()
export class ProductStore {
	private productRest = container.resolve(ProductRest)
	selected: IProduct | null = null
	products: IProduct[] = []

	constructor() {
		makeAutoObservable<ProductStore, 'productRest'>(this, {
			productRest: false
		})
	}

	public async getAllProducts() {
		const { data } = await this.productRest.getAllProducts()
		this.products = data.results
	}

	public async getProduct(id: number) {
		const { data } = await this.productRest.getProduct(id)
		this.selected = data
	}

	setSelected(data: IProduct) {
		this.selected = data
	}
}

export const useProductStore = () => container.resolve(ProductStore)
