// TODO: Add attributes

export interface ICategory {
	name: string
	description?: string
}

export interface IProduct {
	id: number
	productName: string
	description?: string
	cost: string
	category: ICategory
}
