import { IProduct } from '../models/Product'
import { ProductListScreen } from '../pages/ProductListScreen'
import { ProductScreen } from '../pages/ProductScreen'

export type { IProduct }

export {
	// Pages
	ProductListScreen,
	ProductScreen
}
