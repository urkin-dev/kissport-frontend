import styled from 'styled-components'
import { Header } from '@lib/components'
import { COLORS, Typography } from '@kissport/ui'

export const MainScreen = () => {
	// TODO: Find out why can't style ScreenContainer w\h styled-components
	return (
		<StyledMain>
			<Header />
			<IntroContainer>
				<Intro>
					<MainTitle>KISSport</MainTitle>
					<MainDescription>
						Спортивные товары для вас и вашей семьи. Наши товары подойдут как и для профессиональных спортсменов, так и
						для начинающих.
					</MainDescription>
				</Intro>
			</IntroContainer>
			<About>
				<AboutInfo>
					<Title tag="h2" type="h2">
						О нас
					</Title>
					<Description tag="p" type="medium" color={COLORS.GrayBase}>
						Удобная одежда, спортивный инвентарь и другие спортивные товары доступны для вас в пару нажатий. Заказывайте
						у нас и присоединяйтесь к спортивному сообществу, ведь каждому известно что "В здоровом теле - здоровый
						дух"!
					</Description>
				</AboutInfo>
				<AboutImage src="/photos/about.jpg" />
			</About>
		</StyledMain>
	)
}

const MainTitle = styled.h1`
	font-size: 72px;
	color: ${COLORS.White};
	font-weight: bold;
`

const MainDescription = styled.p`
	font-size: 42;
	color: ${COLORS.White};
	font-weight: bold;
	max-width: 500px;
	margin-top: 40px;
	margin-bottom: 40px;
`

const StyledMain = styled.div`
	display: flex;
	flex-direction: column;
	background: linear-gradient(90deg, ${COLORS.GreenBase} 60%, ${COLORS.GrayBase} 60%);
`

const IntroContainer = styled.div`
	padding-left: 150px;
	padding-right: 150px;
	display: flex;
	flex-direction: column;
	height: 100vh;
	padding-bottom: 100px;
`

const Intro = styled.div`
	display: flex;
	flex-direction: column;
	flex: 1;
	justify-content: center;
	padding-left: 40px;
	background: url('/photos/intro.jpg') ${COLORS.GrayDarken} no-repeat;
	background-size: cover;
`

const About = styled.div`
	display: flex;
	padding-left: 150px;
	padding-bottom: 50px;
	padding-right: 150px;
`

const AboutInfo = styled.div`
	padding-right: 50px;
`

const AboutImage = styled.img`
	min-width: 600px;
	max-width: 600px;
`

const Title = styled(Typography)`
	font-size: 42px;
	color: ${COLORS.White};
	font-weight: bold;
	margin-bottom: 50px;
`

const Description = styled(Typography)`
	line-height: 28px;
`
