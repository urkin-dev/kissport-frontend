import { ScreenContainer } from '@lib/components'
import { COLORS, Typography } from '@kissport/ui'
import { NavLink } from 'react-router-dom'
import { RoutesEnum } from '@lib/routes'
import styled from 'styled-components'

export const ErrorScreen = () => {
	return (
		<ScreenContainer>
			<Content>
				<MainTitle tag="h1" type="h3" color={COLORS.RedBase}>
					Произошла ошибка
				</MainTitle>
				<StyledLink to={RoutesEnum.Home}>Вернуться на главную</StyledLink>
			</Content>
		</ScreenContainer>
	)
}

const MainTitle = styled(Typography)`
	margin-bottom: 20px;
`

const Content = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`

const StyledLink = styled(NavLink)`
	color: ${COLORS.GreenBase};
	text-decoration: underline;
`
