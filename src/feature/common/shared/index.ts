import { ErrorScreen } from '../pages/ErrorScreen'
import { MainScreen } from '../pages/MainScreen'

export {
	// Pages
	MainScreen,
	ErrorScreen
}
