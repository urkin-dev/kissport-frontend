import { makeAutoObservable } from 'mobx'
import { AuthRest, ILoginCredentials, ISignCredentials } from '../services'
import { persistentStorage } from '@lib/storage'
import { container, singleton } from 'tsyringe'
import { history } from '@lib/utils'
import { RoutesEnum } from '@lib/routes'
import { UserStore } from '@feature/user'

@singleton()
export class AuthStore {
	private authRest = container.resolve(AuthRest)
	private userStore = container.resolve(UserStore)
	public isAuthenticated = false

	constructor() {
		makeAutoObservable<AuthStore, 'authRest' | 'userStore'>(this, {
			authRest: false,
			userStore: false
		})
	}

	async loginUser(creds: ILoginCredentials) {
		const { data } = await this.authRest.login(creds)

		this.userStore.setUser(data.user)
		persistentStorage.setItem('USER_TOKEN', `Token ${data.accessToken}`)
		persistentStorage.setItem('USER_ID', String(data.user.id))

		history.push(RoutesEnum.Home)

		this.isAuthenticated = true
	}

	async logoutUser() {
		await this.authRest.logout()
		persistentStorage.removeItem('USER_TOKEN')
		persistentStorage.removeItem('USER_ID')
		this.userStore.clear()
		this.isAuthenticated = false
		history.push(RoutesEnum.Home)
	}

	async signUser(creds: ISignCredentials) {
		const { data } = await this.authRest.signup(creds)
		await this.loginUser({ username: data.username, email: data.email, password: creds.password })
	}

	public async restoreSession() {
		const response = await this.authRest.restoreSession()

		if (response?.data.user) {
			this.userStore.setUser(response.data.user)
			this.isAuthenticated = true
		}
	}
}

export const useAuthStore = () => container.resolve(AuthStore)
