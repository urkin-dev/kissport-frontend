import styled from 'styled-components'
import { COLORS } from '@kissport/ui'
import LoginForm from '../components/LoginForm'
import { useState } from 'react'
import RegistrationForm from '../components/RegistrationForm'

type FormType = 'registration' | 'login'

interface IProps {
	type?: FormType
}

export function AuthPage({ type = 'login' }: IProps) {
	const [formType, setFormType] = useState(type)

	const goToRegistrationForm = () => setFormType('registration')
	const goToLoginForm = () => setFormType('login')

	return (
		<StyledMain>
			{formType === 'login' && <LoginForm onChange={goToRegistrationForm} />}
			{formType === 'registration' && <RegistrationForm onChange={goToLoginForm} />}
		</StyledMain>
	)
}

const StyledMain = styled.main`
	background-color: ${COLORS.GrayBase};
	display: flex;
	justify-content: center;
	align-items: center;
	height: 100vh;
	flex: 1;
`
