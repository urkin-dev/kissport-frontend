import { Profile, User } from '@feature/user'
import { BaseRest } from '@lib/services'
import { persistentStorage } from '@lib/storage'
import { singleton } from 'tsyringe'

export interface ILoginCredentials {
	username: string
	email: string
	password?: string
}

interface ILoginResponse {
	accessToken?: string
	user: User
}

export interface ISignCredentials extends ILoginCredentials {
	profile: Profile
}

type SignResponse = ISignCredentials

interface IUserResponse extends ILoginResponse {}

interface ITokenResponse {
	accessToken: string
}

@singleton()
export class AuthRest extends BaseRest {
	async login(creds: ILoginCredentials) {
		try {
			//TODO: Encrypt passwords in the future
			const response = await this.http.post<ILoginResponse>('/login', creds, { withCredentials: true })
			this.http.defaults.headers['Authorization'] = `Token ${response.data.accessToken}`

			return response
		} catch (err) {
			throw err
		}
	}

	async signup(creds: ISignCredentials) {
		try {
			const response = await this.http.post<SignResponse>('/users/', creds)

			return response
		} catch (err) {
			throw err
		}
	}

	async logout() {
		try {
			const response = await this.http.post('/logout')
			delete this.http.defaults.headers.common['Authorization']

			return response
		} catch (err) {
			throw err
		}
	}

	async restoreSession() {
		try {
			const token = await this.refreshToken()
			const user_id = persistentStorage.getItem('USER_ID')

			if (token && user_id) {
				this.http.defaults.headers['Authorization'] = `Token ${token}`
				persistentStorage.setItem('USER_TOKEN', `Token ${token}`)

				const response = await this.http.get<IUserResponse>(`/users/${user_id}`)

				if (response) {
					return response
				}
			}
		} catch (err) {
			throw err
		}
	}

	async refreshToken() {
		try {
			const token = persistentStorage.getItem('USER_TOKEN')

			if (token) {
				delete this.http.defaults.headers['Authorization']
				const response = await this.http.post<ITokenResponse>('/refresh-token', null, { withCredentials: true })

				if (response) {
					return response.data.accessToken
				}
			}
		} catch (err) {
			throw err
		}
	}
}
