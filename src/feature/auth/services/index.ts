import { ISignCredentials, ILoginCredentials } from './AuthRest'
import { AuthRest } from './AuthRest'

export type { ILoginCredentials, ISignCredentials }

export { AuthRest }
