import { useAuthStore } from './../store/AuthStore'
import { ILoginCredentials, ISignCredentials } from '../services'
import { AuthPage } from '../pages'

export type { ILoginCredentials, ISignCredentials }

export {
	// Pages
	AuthPage,
	// Store
	useAuthStore
}
