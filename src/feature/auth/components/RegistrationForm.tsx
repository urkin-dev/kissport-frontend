import { useCallback } from 'react'
import { Form, Input, Button } from 'antd'
import { ISignCredentials } from '../services'
import { Alert, COLORS, FONT, Typography } from '@kissport/ui'
import { useAlert } from '@lib/hooks'
import { useAuthStore } from '../store/AuthStore'
import styled from 'styled-components'
import { HTTPError } from '@lib/services'

interface IRegistrationData extends Omit<ISignCredentials, 'profile'> {
	birthday: string
}

// TODO: Refactor: Merge two forms in one
interface IProps {
	onChange: () => void
}

export default function RegistrationForm({ onChange }: IProps) {
	const authStore = useAuthStore()
	const alert = useAlert()

	const onSubmit = useCallback((data: IRegistrationData) => {
		authStore
			.signUser({
				username: data.username,
				email: data.email,
				password: data.password,
				profile: { birthday: data.birthday }
			})
			.catch((e: HTTPError) => {
				alert.setErrors(e)
				alert.showAlert(e.message)
			})
	}, [])

	return (
		<FormContainer>
			<FormTitle tag="h3" type="h3" color={COLORS.Black}>
				Регистрация
			</FormTitle>
			<Form layout="horizontal" onFinish={onSubmit}>
				<CustomFormItem name="username" rules={[{ required: true, message: '* Имя пользователя обязательно' }]}>
					<CustomInput placeholder="Имя пользователя" type="text" />
				</CustomFormItem>
				<CustomFormItem name="email" rules={[{ required: true, message: '* Email обязателен' }]}>
					<CustomInput placeholder="Email" type="email" />
				</CustomFormItem>
				<CustomFormItem name="password" rules={[{ required: true, message: '* Необходимо ввести пароль' }]}>
					<CustomInput placeholder="Пароль" type="password" />
				</CustomFormItem>
				<CustomFormItem name="birthday" label="Дата рождения">
					<CustomInput placeholder="Дата рождения" type="date" />
				</CustomFormItem>

				{alert.visible && <Alert message={alert.alertMessage} isCritical={alert.errors?.code === 500} />}
				<CustomFormItem>
					<CustomButton htmlType="submit" type="primary">
						Зарегистрироваться
					</CustomButton>
				</CustomFormItem>
				<CustomLinkBtn onClick={onChange} type="button">
					Войти
				</CustomLinkBtn>
			</Form>
		</FormContainer>
	)
}

const FormTitle = styled(Typography)`
	text-align: center;
`

const FormContainer = styled.div`
	display: flex;
	flex-direction: column;
	background-color: ${COLORS.White};
	padding: 20px 40px;
	border-radius: 15px;
	width: 450px;
`

const CustomFormItem = styled(Form.Item)`
	margin-top: 20px;

	.ant-form-item-label {
		margin-bottom: 5px;
	}
`

const CustomInput = styled(Input)`
	font-size: ${FONT.medium}px;
	padding: 10px 12px;
	width: 100%;
	border: 1px solid ${COLORS.GreenLighten};
	border-radius: 5px;
	margin-bottom: 5px;
	transition: border-color 0.3s;

	&:hover {
		border-color: ${COLORS.GreenDarken};
	}

	&:focus {
		outline: none;
		border-color: ${COLORS.GreenDarken};
		box-shadow: none;
	}
`

const CustomButton = styled(Button)`
	background-color: ${COLORS.GreenBase};
	color: ${COLORS.White};
	width: 100%;
	height: 50px;
	border: none;
	border-radius: 7px;
	font-size: ${FONT.large}px;
	transition: opacity 0.3s;
	cursor: pointer;

	&:hover {
		opacity: 0.9;
	}
`

const CustomLinkBtn = styled.button`
	outline: none;
	border: none;
	padding: 0;
	margin: 0;
	margin-top: 10px;
	background-color: transparent;
	cursor: pointer;
	color: ${COLORS.GreenBase};
	font-size: ${FONT.small}px;
	text-decoration: underline;
`
