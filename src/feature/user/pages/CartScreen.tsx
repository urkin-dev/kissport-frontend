import { observer } from 'mobx-react-lite'
import { ScreenContainer } from '@lib/components'
import { Alert, COLORS, FONT, Typography } from '@kissport/ui'
import styled from 'styled-components'
import { useUserStore } from '../store/UserStore'
import { CardCard } from '../components/CartCard'
import { useAlert } from '@lib/hooks'
import { Button } from 'antd'

export const CartScreen = observer(() => {
	const userStore = useUserStore()
	const isEmpty = userStore.cart.length === 0
	const alert = useAlert()

	const submitOrder = () => alert.showAlert('Оформление заказа пока недоступно')

	return (
		<ScreenContainer>
			<Content>
				<MainTitle tag="h1" type="h1" color={COLORS.GreenDarken}>
					Ваш заказ
				</MainTitle>
				{isEmpty ? (
					<Typography tag="p" type="small" color={COLORS.Black}>
						Корзина пустая
					</Typography>
				) : (
					<>
						<ProductList>
							{userStore.cart.map((item) => (
								<CardCard key={item.product.id} data={item} />
							))}
						</ProductList>
						<CustomButton htmlType="button" type="primary" onClick={submitOrder}>
							Оформить заказ
						</CustomButton>
					</>
				)}
			</Content>
			{alert.visible && <Alert message={alert.alertMessage} isCritical={alert.errors?.code === 500} />}
		</ScreenContainer>
	)
})

const MainTitle = styled(Typography)`
	margin-bottom: 10px;
`

const Content = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	padding-left: 40px;
`

const ProductList = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`

const CustomButton = styled(Button)`
	margin-top: 40px;
	background-color: ${COLORS.GreenBase};
	color: ${COLORS.White};
	width: 250px;
	height: 50px;
	border: none;
	border-radius: 7px;
	font-size: ${FONT.large}px;
	transition: background-color 0.5s, opacity 0.3s;
	cursor: pointer;

	&:hover {
		opacity: 0.7;
	}
`
