import { ICartItem } from './../models/Cart'
import { IProduct } from './../../product/models/Product'
import { makeAutoObservable } from 'mobx'
import { container, singleton } from 'tsyringe'
import { User } from '../models/User'
import { history } from '@lib/utils'
import { RoutesEnum } from '@lib/routes'

@singleton()
export class UserStore {
	public user: User | null = null
	public cart: ICartItem[] = []

	constructor() {
		makeAutoObservable(this)
	}

	setUser(user: User) {
		this.user = user
	}

	clear() {
		this.user = null
	}

	addProduct(product: IProduct) {
		if (this.user !== null) {
			const item = this.cart.find((item) => item.product.id === product.id)

			if (item) {
				item.count += 1
			} else {
				this.cart.push({ product, count: 1 })
			}
		} else {
			history.push(RoutesEnum.Auth)
		}
	}

	removeProduct(product: IProduct) {
		if (this.user !== null) {
			const item = this.cart.find((item) => item.product.id === product.id)

			if (item && item.count === 1) {
				this.cart = this.cart.filter((item) => item.product.id !== product.id)
			} else if (item) {
				item.count -= 1
			}
		} else {
			history.push(RoutesEnum.Auth)
		}
	}
}

export const useUserStore = () => container.resolve(UserStore)
