import { IProduct } from './../../product/models/Product'

export interface ICartItem {
	product: IProduct
	count: number
}
