export class Profile {
	birthday?: string
}

export class User {
	static NULL: User = {
		id: 0,
		username: 'Username',
		firstName: 'FirstName',
		lastName: 'LastName',
		email: 'user@example.com',
		profile: {}
	}

	id: number
	username: string
	firstName?: string
	lastName?: string
	email: string
	profile: Profile
}

export interface IUpdateCredentials {
	username: string
	email: string
	firstName: string
	lastName: string
	imageUrl: File
	birthday: Date
}
