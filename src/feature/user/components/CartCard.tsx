import { COLORS, Typography } from '@kissport/ui'
import { Price } from '@lib/utils'
import { observer } from 'mobx-react-lite'
import styled from 'styled-components'
import { ICartItem } from '../models/Cart'
import { useUserStore } from '../store/UserStore'

interface IProps {
	data: ICartItem
}

export const CardCard = observer(({ data }: IProps) => {
	const userStore = useUserStore()

	const removeItem = () => userStore.removeProduct(data.product)
	const addItem = () => userStore.addProduct(data.product)

	return (
		<Container>
			<div>
				<Typography tag="p" type="large" weight="bold">
					{data.product.productName}
				</Typography>
				<Cost tag="p" type="medium">
					{Price.formatToRubles(data.product.cost)}
				</Cost>
			</div>
			<CountContainer>
				<CountButton onClick={removeItem}>-</CountButton>
				<Typography tag="p" type="medium" color={COLORS.GreenDarken}>
					{data.count}
				</Typography>
				<CountButton onClick={addItem}>+</CountButton>
			</CountContainer>
		</Container>
	)
})

const Container = styled.div`
	display: flex;
	margin-top: 15px;
	justify-content: space-between;
	align-items: center;
	border-bottom: 1px solid ${COLORS.GreenBase};
	padding: 15px 15px 15px 0;
	width: 300px;
`

const Cost = styled(Typography)`
	margin-top: 8px;
	color: ${COLORS.GreenBase};
`

const CountContainer = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	border: 1px solid ${COLORS.GreenBase};
	padding: 10px;
	min-width: 100px;
`

const CountButton = styled.a`
	color: ${COLORS.GreenDarken};
	cursor: pointer;
`
