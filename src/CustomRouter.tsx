import { Router } from 'react-router-dom'
import { RouterProps } from 'react-router'
import { BrowserHistory } from 'history'
import React from 'react'

// https://stackoverflow.com/questions/63471931/using-history-with-react-router-dom-v6
interface IProps extends Partial<RouterProps> {
	history: BrowserHistory
}

export const CustomRouter = ({ basename, children, history }: IProps) => {
	const [state, setState] = React.useState({
		action: history.action,
		location: history.location
	})

	React.useLayoutEffect(() => history.listen(setState), [history])

	return (
		<Router
			basename={basename}
			children={children}
			location={state.location}
			navigationType={state.action}
			navigator={history}
		/>
	)
}
