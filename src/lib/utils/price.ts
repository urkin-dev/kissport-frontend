import { trimSpaces } from './fn'

type PriceType = number | string
const DEFAULT_ERROR_ANSWER = 'Ошибка с суммой'

const spacedNumberRegExp = /\B(?=(\d{3})+(?!\d))/g

/**
 * Splits the price with spaces
 */
const formatPrice = (price: PriceType, suffix?: string) => {
	const cleanPrice = trimSpaces(String(price))
	if (cleanPrice !== '' && !Number.isNaN(Number(cleanPrice))) {
		const priceWithSpaces = cleanPrice.replace(spacedNumberRegExp, ' ')
		return suffix ? priceWithSpaces + suffix : priceWithSpaces
	}

	return DEFAULT_ERROR_ANSWER
}

/**
 * Splits the price with spaces and adds '₽' suffix
 */
const formatToRubles = (price: PriceType) => formatPrice(price, ' ₽')

/**
 * Splits the price with spaces and adds 'млн. ₽' suffix
 */
const formatToMillions = (price: PriceType) => formatPrice(price, ' млн. ₽')

/**
 * Splits the price with spaces and adds млн '₽ / м²' suffix
 */
const formatToM2 = (price: PriceType) => formatPrice(price, ' ₽ / м²')

export const Price = {
	formatToRubles,
	formatPrice,
	formatToMillions,
	formatToM2
}
