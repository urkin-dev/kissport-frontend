export const trimSpaces = (val: string) => val.replace(/\s/g, '')
