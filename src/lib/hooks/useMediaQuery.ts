import { useState, useEffect } from 'react'

/**
 * Takes a media query and return true or false if this query matches window width
 * @param query Media query like "(max-width: 768px)"
 */
export default function useMediaQuery(query: string) {
	const [matches, setMatches] = useState(false)

	useEffect(() => {
		const media = window.matchMedia(query)

		if (media.matches !== matches) {
			setMatches(media.matches)
		}

		const listener = () => {
			setMatches(media.matches)
		}

		media.addEventListener('change', listener)

		return () => media.removeEventListener('change', listener)
	}, [query])

	return matches
}
