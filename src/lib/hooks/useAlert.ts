import { HTTPError } from '@lib/services/error'
import { useState } from 'react'

export const useAlert = () => {
	const [visible, setVisible] = useState(false)
	const [errors, setErrors] = useState<HTTPError | null>(null)
	const [alertMessage, setAlertMessage] = useState('')

	const hideAlert = () => {
		setTimeout(() => {
			setVisible(false)
		}, 3000)
	}
	const showAlert = (message: string) => {
		setVisible(true)
		setAlertMessage(message)
		hideAlert()
	}

	return {
		visible,
		showAlert,
		hideAlert,
		errors,
		setErrors,
		alertMessage
	}
}
