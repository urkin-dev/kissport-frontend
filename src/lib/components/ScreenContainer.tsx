import { COLORS } from '@kissport/ui'
import styled from 'styled-components'
import { Header } from './header'

interface IProps {
	children?: React.ReactNode
}

export const ScreenContainer = ({ children }: IProps) => {
	return (
		<Container>
			<Header />
			<Content>{children}</Content>
		</Container>
	)
}

const Container = styled.div`
	display: flex;
	flex-direction: column;
	flex: 1;
	background-color: ${COLORS.GrayBase};
`

const Content = styled.div`
	padding-right: 40px;
	padding-top: 20px;
	padding-bottom: 30px;
`
