import { useAuthStore } from '@feature/auth'
import { useUserStore } from '@feature/user'
import { COLORS, Typography } from '@kissport/ui'
import { RoutesEnum } from '@lib/routes'
import { history } from '@lib/utils'
import { observer } from 'mobx-react-lite'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

export const Header = observer(() => {
	const authStore = useAuthStore()
	const userStore = useUserStore()

	const isAuthenticated = authStore.isAuthenticated

	const logout = () => authStore.logoutUser()
	const login = () => history.push(RoutesEnum.Auth)
	const goToCart = () => history.push(RoutesEnum.CartScreen)

	return (
		<Container>
			<NavBar>
				<StyledLink to={RoutesEnum.Home}>
					<Typography tag="h4" type="h4" color={COLORS.White}>
						Главная
					</Typography>
				</StyledLink>
				<StyledLink to={RoutesEnum.ProductsList}>
					<Typography tag="h4" type="h4" color={COLORS.White}>
						Каталог
					</Typography>
				</StyledLink>
			</NavBar>
			<RightContainer>
				{isAuthenticated && userStore.user && (
					<>
						<Typography tag="p" type="large" color={COLORS.White}>
							{userStore.user.username}
						</Typography>
						<Cart onClick={goToCart}>Корзина</Cart>
					</>
				)}
				<Logout onClick={isAuthenticated ? logout : login}>{isAuthenticated ? 'Выйти' : 'Войти'}</Logout>
			</RightContainer>
		</Container>
	)
})

const Container = styled.div`
	display: flex;
	align-items: center;
	padding-right: 40px;
	width: 100%;
	height: 100px;
	background-color: ${COLORS.GreenBase};
`

const NavBar = styled.nav`
	padding: 0;
	display: flex;
	margin-left: 150px;
`
const StyledLink = styled(NavLink)`
	margin-right: 50px;
	outline: none;

	/* TODO: Refactor */
	&.active h4 {
		border-bottom: 2px solid ${COLORS.White};
	}
`

const RightContainer = styled.div`
	display: flex;
	flex: 1;
	align-items: center;
	justify-content: flex-end;
`

const Cart = styled.a`
	margin-left: 10px;
	color: ${COLORS.White};
	cursor: pointer;
	font-weight: bold;
`

const Logout = styled.a`
	margin-left: 100px;
	color: ${COLORS.White};
	text-decoration: underline;
	cursor: pointer;
`
