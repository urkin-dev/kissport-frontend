export const persistentStorage =
	!process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? sessionStorage : localStorage
