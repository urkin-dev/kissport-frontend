export enum RoutesEnum {
	// Main pages
	Auth = '/auth', // Landing page with auth
	Home = '/', // Home page after auth

	// All pages
	ProductsList = '/products',
	ProductScreen = '/product',
	CartScreen = '/cart',

	// Error pages
	Error = '/error'
}
