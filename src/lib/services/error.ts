/**
 * Class описывающий HTTP ошибку
 * @class
 */

export class HTTPError extends Error {
	public code: number
	public message: string
	public details: string

	constructor(code = 500, message = 'Unknown error', details = '') {
		super()
		this.code = code
		this.message = message
		this.details = details
	}
}
