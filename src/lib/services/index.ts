import BaseRest, { IPagination } from './BaseRest'
import { HTTPError } from './error'

export type { IPagination }

export { BaseRest, HTTPError }
