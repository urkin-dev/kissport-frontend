import { RoutesEnum } from '@lib/routes'
import { persistentStorage } from '@lib/storage'
import axios, { AxiosError, AxiosResponse } from 'axios'
import { HTTPError } from './error'
import { history } from '../utils'

export interface IPagination<T> {
	count: number
	next: string | null
	previous: string | null
	total_pages: number
	page: number
	limit: number
	results: T[]
}

export type HttpResponse<T = object> = Promise<AxiosResponse<T>>

const Api = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
	timeout: 5000
})

Api.interceptors.request.use((request) => {
	// We don't need Authorization header when refresh-token goes or it will be error
	if (request.headers.common['Authorization'] && request.url === '/refresh-token') {
		delete request.headers.common['Authorization']
	}

	return request
})

// TODO: Make normal error handling
Api.interceptors.response.use(
	(response) => response,
	(error: AxiosError) => {
		if (error.response && error.response.status) {
			let title = String(error.response.status)
			let status = String(error.response.status)

			switch (error.response.status) {
				case 500:
					title = 'Пожалуйста, обновите страницу'
					break
				case 404:
					title = 'Не найдено'
					break
				case 403:
					title = 'Нет доступа'
					break
				case 401:
					title = 'Ошибка доступа'
					break
				case 409:
					title = 'Невалидные данные'
					break
				case 400:
					title = 'Невалидные данные'
					break
			}

			if (error.response.status !== 403 && (error.response.status === 500 || error.response.status === 404)) {
				history.push(
					{
						pathname: RoutesEnum.Error
					},
					{
						state: {
							error: {
								title,
								status
							}
						}
					}
				)
				return error.response
			} else if (error.response.status !== 403) {
				const newError = new HTTPError(Number(status), title)
				return Promise.reject(newError)
			}

			delete Api.defaults.headers['Authorization']
			return Api.post('/refresh-token', null, { withCredentials: true })
				.then((response) => {
					console.log('Token updated')
					// Set token in default headers
					Api.defaults.headers['Authorization'] = `Token ${response.data.accessToken}`
					persistentStorage.setItem('USER_TOKEN', `Token ${response.data.accessToken}`)
					// Make response and return the data
					error.config.headers['Authorization'] = `Token ${response.data.accessToken}`
					return Api.request(error.config)
				})
				.catch((e) => {
					console.log(e)
				})
		} else {
			history.push(
				{
					pathname: RoutesEnum.Error
				},
				{
					state: {
						error: {
							title: 'Server is sleeping right now, try again to wake it up',
							status: 500
						}
					}
				}
			)
		}
	}
)

export default abstract class BaseRest {
	readonly http = Api

	protected async putObject<T, R>(data: T, url: string) {
		try {
			const response = await Api.put<R>(url, data, { withCredentials: true })

			if (response) {
				return response
			} else {
				return null
			}
		} catch (err) {
			throw err
		}
	}

	protected async deleteObject<T>(data: T, url: string) {
		try {
			const response = await Api.delete<T>('/users/', { data, withCredentials: true })

			if (response) {
				return response
			} else {
				return null
			}
		} catch (err) {
			throw err
		}
	}

	protected async getObjects<T>(url: string) {
		try {
			const response = await Api.get<T[]>(url)

			if (response) {
				return response
			} else {
				return null
			}
		} catch (err) {
			throw err
		}
	}

	protected async getPaginatedObjects<T>(url: string) {
		try {
			const response = await Api.get<IPagination<T>>(url, { withCredentials: true })

			if (response) {
				return response
			} else {
				return null
			}
		} catch (err) {
			throw err
		}
	}

	protected async postObject<T>(data: T, url: string) {
		try {
			const response = await Api.post<T>(url, data)

			if (response) {
				return response
			} else {
				return null
			}
		} catch (err) {
			throw err
		}
	}

	protected async postObjectWithCredentials<T>(data: T, url: string) {
		try {
			const response = await Api.post<T>(url, data, { withCredentials: true })

			if (response) {
				return response
			} else {
				return null
			}
		} catch (err) {
			throw err
		}
	}
}
