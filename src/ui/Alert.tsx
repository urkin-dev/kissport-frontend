import styled from 'styled-components'
import { COLORS } from './theme'

interface IProps {
	message: string
	isCritical: boolean
}

export default function Alert({ message, isCritical }: IProps) {
	return (
		<AlertBox data-critical={isCritical ? 'critical' : 'common'}>
			<AlertMessage>{message}</AlertMessage>
		</AlertBox>
	)
}

const AlertBox = styled.div`
	&[data-critical='critical'] {
		background-color: ${COLORS.RedBase};
	}

	&[data-critical='common'] {
		background-color: ${COLORS.Black};
		opacity: 0.6;
	}

	line-height: 1.5;
	position: fixed;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	padding: 8px 14px;
	min-width: 200px;
	max-width: 60%;
	display: flex;
	justify-content: center;
	align-items: center;
	z-index: 9999;
	border-radius: 4px;
	cursor: pointer;
`

const AlertMessage = styled.p`
	color: #fff;
	padding: 0;
	margin: 0;
	text-align: center;
`
