export const COLORS = {
	Black: '#000000',
	White: '#ffffff',
	GrayBase: '#E7E8E3',
	GrayDarken: '#dee0db',
	// Green
	GreenBase: '#6F8779',
	GreenLighten: '#73B58E',
	GreenDarken: '#3D4639',
	// Red
	RedBase: '#EB5757',
	RedLighten20: '#FDEFEF'
}

export const FONT = {
	h1: 26,
	h2: 24,
	h3: 22,
	h4: 20,
	large: 18,
	medium: 16,
	small: 14,
	tiny: 12
}
