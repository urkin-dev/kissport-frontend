import { COLORS, FONT } from './theme'
import Alert from './Alert'
import { Typography } from './Typography'

export { COLORS, FONT, Alert, Typography }
