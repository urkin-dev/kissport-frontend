import React, { useMemo } from 'react'

export type TypographyTag = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p' | 'span'
export type TypographyType = 'h1' | 'h2' | 'h3' | 'h4' | 'large' | 'small' | 'medium' | 'tiny'
const TYPOGRAPHY_HEADINGS: TypographyType[] = ['h1', 'h2', 'h3', 'h4']
const isHeading = (type: TypographyType) => TYPOGRAPHY_HEADINGS.includes(type)

export type TypographyFontWeight = 'regular' | 'accent' | 'bold'

interface ITextProps {
	tag: TypographyTag
	children: React.ReactNode
	classname?: string
	style?: React.CSSProperties
}
const Text = ({ tag, children, classname, style }: ITextProps) => {
	const TagName = `${tag}`

	return (
		// @ts-expect-error
		<TagName style={style} className={classname}>
			{children}
		</TagName>
	)
}

interface IProps {
	tag: TypographyTag
	type: TypographyType
	maxLength?: number
	weight?: TypographyFontWeight
	children: string | React.ReactNode
	color?: string
	style?: React.CSSProperties
	className?: string
}

export const Typography = ({ tag, type, maxLength, weight = 'regular', color, children, style, className }: IProps) => {
	const fontTypeStyle = ssFontType[type]
	const fontWeight = isHeading(type) && weight === 'regular' ? 'bold' : weight

	const content = useMemo(() => {
		if (typeof children === 'string' && maxLength) {
			return children.length <= maxLength ? children : `${children.substring(0, maxLength)}...`
		} else {
			return children
		}
	}, [children, maxLength])

	return (
		<Text tag={tag} classname={className} style={{ ...fontTypeStyle, ...style, color, ...ssFontWeight[fontWeight] }}>
			{content}
		</Text>
	)
}

const ssFontWeight: Record<TypographyFontWeight, React.CSSProperties> = {
	regular: {
		fontWeight: 400
	},
	accent: {
		fontWeight: 500
	},
	bold: {
		fontWeight: 500
	}
}

const ssFontType: Record<TypographyType, React.CSSProperties> = {
	h1: {
		fontSize: 48
	},
	h2: {
		fontSize: 42
	},
	h3: {
		fontSize: 34
	},
	h4: {
		fontSize: 20
	},
	large: {
		fontSize: 18
	},
	medium: {
		fontSize: 16
	},
	small: {
		fontSize: 14
	},
	tiny: {
		fontSize: 12
	}
}
