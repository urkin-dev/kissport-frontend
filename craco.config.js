const CracoAliasPlugin = require('craco-alias')

module.exports = {
	plugins: [
		{
			plugin: CracoAliasPlugin,
			options: {
				source: 'tsconfig',
				baseUrl: './src',
				tsConfigPath: './tsconfig.paths.json'
			}
		}
	],
	babel: {
		plugins: [
			['@babel/plugin-proposal-decorators', { legacy: true }],
			[
				'babel-plugin-styled-components',
				{
					ssr: false,
					pure: true
				}
			]
		]
	}
}
